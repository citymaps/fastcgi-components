//
//  Response.cpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/20/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#include <sstream>

#include "Response.h"

namespace fcgicomps
{
    std::vector<uint8_t> Response::ToByteArray()
    {
        std::stringstream str(std::stringstream::out | std::stringstream::binary);
        
        str << "Status: " << mStatus << "\r\n";
        for (auto& header : mHeaders) {
            str << header.first << ": " << header.second << "\r\n";
        }
        str << "\r\n";
        
        auto header = str.str();

        std::vector<uint8_t> output;
        output.resize(header.size() + mBody.size());
        
        std::copy(header.begin(), header.end(), output.begin());
        std::copy(mBody.begin(), mBody.end(), output.begin() + header.size());
        
        return output;
    }
    
    void Response::SetCompletionCallback(std::function<void()> callback)
    {
        mCallback = callback;
        
        this->NotifyCallback();
    }
    
    void Response::Send()
    {
        mSent = true;
        
        this->NotifyCallback();
    }
    
    void Response::NotifyCallback()
    {
        if (mSent && mCallback) {
            mCallback();
        }
    }
}
