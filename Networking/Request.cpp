//
//  Request.cpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/20/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#include "Request.h"

#include <vector>
#include <boost/algorithm/string.hpp>

#include <curl/curl.h>

namespace fcgicomps
{
    Request::Request(FCGX_Request* fcgiRequest)
    {
        mRequestUri = FCGX_GetParam("REQUEST_URI", fcgiRequest->envp);

        // Strip out possible query string parameters
        auto paramStart = mRequestUri.find_first_of('?');
        if (paramStart != std::string::npos) {
            mUri = mRequestUri.substr(0, paramStart);
        } else {
            mUri = mRequestUri;
        }

        mMethod = FCGX_GetParam("REQUEST_METHOD", fcgiRequest->envp);
        boost::algorithm::to_upper(mMethod);

        std::string queryString = FCGX_GetParam("QUERY_STRING", fcgiRequest->envp);
        
        if (mMethod == "POST" || mMethod == "PUT") {
            int contentLength = atoi(FCGX_GetParam("CONTENT_LENGTH", fcgiRequest->envp));
            std::string postContent;
            postContent.resize(contentLength);
            FCGX_GetStr((char *)postContent.data(), (int) postContent.size(), fcgiRequest->in);
            
            std::string contentType = FCGX_GetParam("CONTENT_TYPE", fcgiRequest->envp);
            boost::algorithm::to_lower(contentType);
            if (contentType == "application/x-www-form-urlencoded") {
                this->ParseQueryParams(postContent);
            }
        }
        
        this->ParseQueryParams(queryString);
    }
    
    void Request::ParseQueryParams(const std::string& queryString)
    {
        std::vector<std::string> queryPairs;
        
        boost::split(queryPairs, queryString, boost::is_any_of("&"));

        for (auto& queryPair : queryPairs) {
            std::vector<std::string> queryPairParts;
            boost::split(queryPairParts, queryPair, boost::is_any_of("="));
            if (queryPairParts.size() == 2) {
                std::string key = curl_easy_unescape(NULL, queryPairParts[0].c_str(),
                                                     queryPairParts[0].length(), NULL);
                boost::algorithm::to_lower(key);

                std::string value = curl_easy_unescape(NULL, queryPairParts[1].c_str(),
                                                     queryPairParts[1].length(), NULL);

                this->AddQueryParam(key, value);
            }
        }
    }

    const std::vector<std::string> Request::GetQueryParam(const std::string& param)
    {
        auto it = mQueryParams.find(boost::algorithm::to_lower_copy(param));
        if (it != mQueryParams.end()) {
            return it->second;
        }

        return std::vector<std::string>();
    }

    const std::string Request::GetFirstQueryParam(const std::string& param)
    {
        auto it = mQueryParams.find(boost::algorithm::to_lower_copy(param));
        if (it != mQueryParams.end() && !it->second.empty()) {
            return it->second.front();
        }

        return std::string();
    }
}
