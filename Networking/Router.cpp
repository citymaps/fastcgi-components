//
//  Router.cpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/20/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#include "Router.h"

#include <boost/algorithm/string.hpp>

namespace fcgicomps
{
    void Router::RegisterRoute(const std::string& method, const std::string& route, TRouteCallback callback)
    {
        RouterCallback cb = {method, route, callback};
        mRoutes.push_back(cb);
    }
    
    TRouteCallback Router::FindMatchingRoute(Request* req)
    {
        std::string uriWithoutQuery = req->GetUri().substr(0, req->GetUri().find_first_of('?'));
        
        std::vector<std::string> urlParts;
        boost::split(urlParts, uriWithoutQuery, boost::is_any_of("/"));
        
        for (auto& route : mRoutes) {
            if (route.method == req->GetMethod()) {
                std::vector<std::string> routeUrlParts;
                boost::split(routeUrlParts, route.pattern, boost::is_any_of("/"));

                if (urlParts.size() >= routeUrlParts.size()) {
                    bool matches = true;
                    
                    for (size_t i = 0; i < routeUrlParts.size(); i++) {
                        std::string& routePart = routeUrlParts[i];
                        std::string& urlPart = urlParts[i];
                        
                        if (routePart != urlPart && routePart[0] != ':' && routePart[0] != '*') {
                            // Parts don't match and it's not a parameter, so this doesnt match
                            matches = false;
                            break;
                        }
                        
                        if (routePart[0] == ':') {
                            req->AddQueryParam(routePart.substr(1), urlPart);
                        }
                    }
                    
                    if (matches) {
                        return route.callback;
                    }
                }
            }
        }
        
        return nullptr;
    }
}
