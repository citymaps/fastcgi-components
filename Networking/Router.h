//
//  Router.hpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/20/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#pragma once

#include <memory>
#include <functional>
#include <unordered_map>
#include <vector>

#include "Request.h"
#include "Response.h"

namespace fcgicomps
{
    typedef std::function<void(Request*, Response*)> TRouteCallback;
    
    struct RouterCallback
    {
        std::string method;
        std::string pattern;
        TRouteCallback callback;
    };
    
    class Router
    {
    public:
        void RegisterRoute(const std::string& method, const std::string& route, TRouteCallback callback);
        TRouteCallback FindMatchingRoute(Request* req);
        
    private:
        std::vector<RouterCallback> mRoutes;
    };
}
