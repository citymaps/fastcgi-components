//
//  Response.hpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/20/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#pragma once

#include <functional>
#include <unordered_map>
#include <string>
#include <vector>

namespace fcgicomps
{
    class Response
    {
    public:
        ~Response()
        {
        }
        
        void SetStatus(int status)
        {
            mStatus = status;
        }
        
        void SetBody(const std::string& body)
        {
            mBody.resize(body.length());
            std::copy(body.begin(), body.end(), mBody.begin());
        }
        
        void SetBody(const std::vector<uint8_t>& body)
        {
            mBody = std::move(body);
        }
        
        void SetHeader(const std::string& key, const std::string& value)
        {
            mHeaders[key] = value;
        }
        
        void SetContentType(const std::string& contentType)
        {
            mHeaders["Content-Type"] = contentType;
        }
        
        void SetCompletionCallback(std::function<void()> callback);
        
        void Send();
        
        std::vector<uint8_t> ToByteArray();
        
    private:
        std::unordered_map<std::string, std::string> mHeaders;
        std::vector<uint8_t> mBody;
        int mStatus;
        std::function<void()> mCallback;
        bool mSent = false;
        
        void NotifyCallback();
    };
}
