//
//  Request.hpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/20/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <fcgio.h>

namespace fcgicomps
{
    static const std::string kEmptyQueryParam;
    
    class Request
    {
    public:
        Request(FCGX_Request* fcgiRequest);
        
        const std::vector<std::string> GetQueryParam(const std::string& param);
        const std::string GetFirstQueryParam(const std::string& param);
        
        void AddQueryParam(const std::string& key, const std::string& value)
        {
            mQueryParams[key].push_back(value);
        }

        /**
         * Get the full request URI, with possible query string parameters
         * @return Full Request URI
         */
        const std::string& GetRequestUri() { return mRequestUri; }

        /**
         * Get only the request path, with possible query parameters stripped out
         * @return Request URI without query parameters
         */
        const std::string& GetUri() { return mUri; }

        const std::string& GetMethod() { return mMethod; }
        
    private:
        std::string mMethod;
        std::string mRequestUri;
        std::string mUri;
        std::unordered_map<std::string, std::vector<std::string>> mQueryParams;
        
        void ParseQueryParams(const std::string& query);
    };
}
