//
//  RequestQueue.hpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/20/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#pragma once

#include <map>
#include <fcgio.h>
#include <async++.h>

#include "Router.h"

namespace fcgicomps
{
    class Response;
    
    class RequestDispatcher
    {
    public:
        RequestDispatcher(int numThreads, Router* router);
        
        void AddRequest(FCGX_Request* request, int socket);
        
    private:
        Router* mRouter;
        async::threadpool_scheduler mThreadPool;
        
        std::map<Response*, FCGX_Request*> mActiveRequests;
        std::mutex mActiveRequestsMutex;
    };
}
