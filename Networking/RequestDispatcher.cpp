//
//  RequestQueue.cpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/20/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#include "RequestDispatcher.h"
#include "Router.h"
#include "Exceptions.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <iostream>

using ptree = boost::property_tree::ptree;

namespace fcgicomps
{
    static void setErrorResponse(Response* response, HTTPStatus status_code, std::string reason = "")
    {
        response->SetStatus(status_code);
        response->SetContentType("application/json");
        
        ptree doc;
        doc.add("status", status_code);
        
        if (reason.length() == 0) {
            reason = GetReasonForStatus(status_code);
        }
        doc.add("reason", reason);
        
        std::ostringstream stringBuffer;
        boost::property_tree::write_json(stringBuffer, doc);
        
        response->SetBody(stringBuffer.str());
        response->Send();
    }
    
    RequestDispatcher::RequestDispatcher(int numThreads, Router* router):
        mRouter(router),
        mThreadPool(numThreads)
    {
    }
    
    void RequestDispatcher::AddRequest(FCGX_Request* request, int socket)
    {
        async::spawn(mThreadPool, [this, request, socket]() {
            auto start = std::chrono::high_resolution_clock::now();
            
            auto req = new Request(request);
            
            auto res = new Response();
            mActiveRequestsMutex.lock();
            mActiveRequests[res] = request;
            mActiveRequestsMutex.unlock();
            
            auto routeCallback = mRouter->FindMatchingRoute(req);
            if (routeCallback) {
                try {
                    auto routeStart = std::chrono::high_resolution_clock::now();
                    routeCallback(req, res);
                    auto routeEnd = std::chrono::high_resolution_clock::now();
                    std::chrono::duration<double, std::milli> elapsed = routeEnd-routeStart;
                    //std::cout << "Route time: " << (elapsed.count()) << "\n";
                } catch (const RequestException& e) {
                    setErrorResponse(res, e.GetStatus(), e.GetReason());
                } catch (...) {
                    // Catch-all, return basic 500
                    setErrorResponse(res, HTTPStatus::HTTP_INTERNAL_SERVER_ERROR);
                }
            } else {
                // Return 404
                setErrorResponse(res, HTTPStatus::HTTP_NOT_FOUND);
            }
            
            res->SetCompletionCallback([this, req, res, start, socket]() {
                res->SetHeader("Access-Control-Allow-Origin", "*");
                res->SetHeader("Access-Control-Allow-Methods", "GET, POST");

                mActiveRequestsMutex.lock();
                auto request = mActiveRequests[res];
                mActiveRequestsMutex.unlock();
                
                auto output = res->ToByteArray();
                FCGX_PutStr((char *)output.data(), (int) output.size(), request->out);
                
                FCGX_Finish_r(request);
                
                auto end = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double, std::milli> elapsed = end-start;
                std::cout << "Total time: " << (elapsed.count()) << "\n";

                mActiveRequestsMutex.lock();
                mActiveRequests.erase(res);
                mActiveRequestsMutex.unlock();
                
                FCGX_Free(request, socket);
                delete request;
                delete req;
                delete res;
            });
        });
    }
}
