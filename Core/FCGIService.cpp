//
//  FCGIService.cpp
//  CitymapsRasterMap
//
//  Created by Adam Eskreis on 5/30/17.
//  Copyright © 2017 Citymaps. All rights reserved.
//

#include "FCGIService.h"
#include "Configuration.h"
#include "Exceptions.h"

namespace fcgicomps
{
    FCGIService::FCGIService(const std::string& configFile, int port):
        mPort(port)
    {
        Configuration::Init(configFile);
        
        mRouter = std::make_unique<Router>();
        
        // Register cross origin resource sharing
        mRouter->RegisterRoute("OPTIONS", "*", [](Request* req, Response* res) {
            res->SetStatus(HTTPStatus::HTTP_OK);
            res->Send();
        });
        
        auto numWorkers = Configuration::GetInt("maxConcurrentRequests");
        if (numWorkers == 0) {
            throw "[maxConcurrentRequests] key must be set in configuration";
        }
        
        mDispatcher = std::make_unique<RequestDispatcher>(numWorkers, mRouter.get());
    }
    
    FCGIService::~FCGIService()
    {
    }
    
    void FCGIService::Start()
    {
        int listenSocket = 0;
        if (mPort > 0) {
            listenSocket = FCGX_OpenSocket(":8000", 400);
        }
        
        int status = FCGX_Init();
        
        if (status == 0) {
            while (true) {
                auto request = new FCGX_Request;
                
                FCGX_InitRequest(request, listenSocket, 0);
                
                if (FCGX_Accept_r(request) == 0) {
                    mDispatcher->AddRequest(request, listenSocket);
                }
            }
        }
    }
}
