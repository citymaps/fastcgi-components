//
//  Configuration.cpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/21/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#include "Configuration.h"

#include <unordered_map>
#include <hocon/config.hpp>

namespace fcgicomps {
    static hocon::shared_config sConfig;
    
    void Configuration::Init(const std::string& filepath)
    {
        LoadConfig(filepath);
    }
    
    bool Configuration::KeyExists(const std::string& key)
    {
        return sConfig->has_path_or_null(key) && !sConfig->get_is_null(key);
    }
    
    std::string Configuration::GetString(const std::string& key, const std::string& defaultValue)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_string(key);
        }
        
        return defaultValue;
    }
    
    int Configuration::GetInt(const std::string& key, int defaultValue)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_int(key);
        }
        
        return defaultValue;
    }
    
    int64_t Configuration::GetLong(const std::string& key, int64_t defaultValue)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_long(key);
        }
        
        return defaultValue;
    }
    
    double Configuration::GetDouble(const std::string& key, double defaultValue)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_double(key);
        }
        
        return defaultValue;
    }
    
    bool Configuration::GetBool(const std::string& key, bool defaultValue)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_bool(key);
        }
        
        return defaultValue;
    }
    
    std::vector<std::string> Configuration::GetStringList(const std::string& key)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_string_list(key);
        }
        
        return std::vector<std::string>();
    }
    
    std::vector<int> Configuration::GetIntList(const std::string& key)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_int_list(key);
        }
        
        return std::vector<int>();
    }
    
    std::vector<int64_t> Configuration::GetLongList(const std::string& key)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_long_list(key);
        }
        
        return std::vector<int64_t>();
    }
    
    std::vector<double> Configuration::GetDoubleList(const std::string& key)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_double_list(key);
        }
        
        return std::vector<double>();
    }
    
    std::vector<bool> Configuration::GetBoolList(const std::string& key)
    {
        if (sConfig->has_path_or_null(key) && !sConfig->get_is_null(key)) {
            return sConfig->get_bool_list(key);
        }
        
        return std::vector<bool>();
    }
    
    void Configuration::LoadConfig(const std::string& file)
    {
        sConfig = hocon::config::parse_file_any_syntax(file)->resolve();
    }
}
