//
//  Exceptions.h
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/20/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#pragma once

#include "HTTPStatus.h"

namespace fcgicomps
{
    struct RequestException
    {
        virtual HTTPStatus GetStatus() const = 0;
        virtual std::string GetReason() const = 0;
    };

    struct GenericRequestException : RequestException
    {
        GenericRequestException(HTTPStatus _status, const std::string& _reason = ""):
                status(_status), reason(_reason) {}

        std::string reason;
        HTTPStatus status;

        HTTPStatus GetStatus() const { return status; }
        std::string GetReason() const { return reason; }
    };

    struct ParameterMissingException : RequestException
    {
        ParameterMissingException(const std::string& _parameter):
                parameter(_parameter) {}

        std::string parameter = "";

        std::string GetReason() const
        {
            return "The following required parameter is missing: [" + parameter + "]";
        }

        HTTPStatus GetStatus() const
        {
            return HTTPStatus::HTTP_BAD_REQUEST;
        }
    };

    struct InvalidParameterException : RequestException
    {
        InvalidParameterException(const std::string& _parameter, const std::string& _expectedType):
                parameter(_parameter),
                expectedType(_expectedType)
        {};

        std::string parameter = "";
        std::string expectedType = "";

        std::string GetReason() const
        {
            return "The following parameter has the wrong type: [" + parameter + "], expected: [" + expectedType + "]";
        }

        HTTPStatus GetStatus() const
        {
            return HTTPStatus::HTTP_BAD_REQUEST;
        }
    };
}

