//
//  FCGIService.hpp
//  CitymapsRasterMap
//
//  Created by Adam Eskreis on 5/30/17.
//  Copyright © 2017 Citymaps. All rights reserved.
//

#pragma once

#include "Router.h"
#include "RequestDispatcher.h"

#include <memory>
#include <string>

namespace fcgicomps
{
    class FCGIService
    {
    public:
        FCGIService(const std::string& configFile, int port = 0);
        ~FCGIService();
        
        Router& GetRouter() { return *mRouter; }
        
        void Start();
        
    private:
        std::unique_ptr<Router> mRouter;
        std::unique_ptr<RequestDispatcher> mDispatcher;
        int mPort;
    };
}
