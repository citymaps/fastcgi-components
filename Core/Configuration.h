//
//  Configuration.hpp
//  MapGlyphService
//
//  Created by Adam Eskreis on 7/21/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#pragma once

#include <string>
#include <vector>

namespace fcgicomps
{
    class Configuration
    {
    public:
        static void Init(const std::string& file);
        
        static std::string GetString(const std::string& key, const std::string& defaultValue = "");
        static int GetInt(const std::string& key, int defaultValue = 0);
        static int64_t GetLong(const std::string& key, int64_t defaultValue = 0);
        static double GetDouble(const std::string& key, double defaultValue = 0);
        static bool GetBool(const std::string& key, bool defaultValue = false);
        
        static bool KeyExists(const std::string& key);
        
        static std::vector<std::string> GetStringList(const std::string& key);
        static std::vector<int> GetIntList(const std::string& key);
        static std::vector<int64_t> GetLongList(const std::string& key);
        static std::vector<double> GetDoubleList(const std::string& key);
        static std::vector<bool> GetBoolList(const std::string& key);
        
    private:
        static void LoadConfig(const std::string& file);
    };
}
